package sg.bigo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "index")
public class IndexController {


    @Value("${test.dio}")
    private String dioTest;

    @RequestMapping(value = "test")
    public String test(){
        return "I am " + dioTest;
    }

    @RequestMapping(value = "add")
    public String add(int a , int b){
        return "result: " + (a+b);
    }

}
